﻿// create the module and name it todoApp
var todoApp = angular.module('todoApp', ['ui.bootstrap', 'ui.bootstrap.datetimepicker', 'ngRoute', 'ngTable', 'ngResource']);

var MainUrl = 'http://localhost:3030/api/Todo/:id';
var MainUrlWithId = 'http://localhost:3030/api/Todo/:id';

var UiUrl = 'http://localhost:5050';

var todoEditdata = null;

var relaod = null;

todoApp.factory('apiService', ['$resource', function ($resource) {
    return $resource(MainUrlWithId, {
                        id: '@id'
                    }, {
                        query: {
                            isArray: false
                        },
                        update: {
                            method: 'PUT'
                        }
                    });
    }
]);

todoApp.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}
]);


// configure our routes
todoApp.config(function ($routeProvider) {
    $routeProvider

        // route for the home page
        .when('/', {
            templateUrl: 'pages/home.html',
            controller: 'mainController',
            reloadOnSearch: true
        })

        // route for the edit page
        .when('/edit', {
            templateUrl: 'pages/edit.html',
            controller: 'editController',
            reloadOnSearch: true
        })

        // route for the about page
        .when('/about', {
            templateUrl: 'pages/about.html',
            controller: 'aboutController',
            reloadOnSearch: true
        });
});

// create the controller and inject Angular's $scope
todoApp.controller('mainController', ['$scope', '$location', 'apiService', '$resource', '$window', function ($scope, $location, apiService, $resource, $window) {

    console.log("TEST");
    
    apiService.query(function (response) {
        console.log("GETALL======>");
        console.log(response.Data);
        $scope.data = response.Data;
    });

    $scope.modify = function(tableData) {
        console.log("EDIT SELECTED ITEM======>");
        console.log(tableData);
        todoEditdata = tableData;

        var t = apiService.get({ id: todoEditdata.TodoId }, function(response) {
            console.log("EDIT GETBYID===>");
            console.log(response.Data);
            todoEditdata = tableData;
            $location.path("/edit");
        });
    }

    $scope.addnew = function () {
        console.log("NEW ITEM======>");
        todoEditdata = {
            TodoId : null,
            Name: '',
            Content: '',
            DueDateTime: new Date().toISOString(),
            IsCompleted : false
        };
        $location.path("/edit");
    }

    $scope.delete = function (tableData) {
        console.log("DELETE SELECTED ITEM======>");
        console.log(tableData);
        todoEditdata = tableData;

        var t = $resource(MainUrlWithId, {
            id: todoEditdata.TodoId
        }, { delete: { method: 'DELETE', data: {}, isArray: false } });

        t.delete().$promise.then(
                function (value) { $window.location.reload(); },
                function (error) { /*Do something with error*/ }
        );
    }

    $scope.message = 'Todo List Home Page';
}]);

todoApp.controller('editController', ['$scope', '$location', 'apiService', '$resource', function ($scope, $location, apiService, $resource) {

    console.log("todoEditdata===>");
    console.log(todoEditdata.TodoId);

    // NEW/EDIT DATA LOAD
    if (todoEditdata != null) {
        console.log("GETBYID FROM EDIT PAGE BEGIN===>");

        if (todoEditdata.TodoId != null) {
            var t = apiService.get({ id: todoEditdata.TodoId }, function(response) {
                console.log("GETBYID FROM EDIT PAGE RESULT PAGE ===>");
                console.log(response.Data);
                $scope.data = response.Data;
            });
        } else {
            console.log("NEW DATA:");
            console.log(todoEditdata);
            $scope.data = todoEditdata;
        }
    }
    //CANCEL

    $scope.cancel = function () {
        $location.path('/');
        
    }

    // NEW/EDIT DATA SAVE

    $scope.save = function (savedata) {
        console.log("SAVE ACTION DATA===>");
        console.log(savedata);
        var t;
        if (savedata.TodoId == null) {
            t = $resource(MainUrl, {}, {
                postM: { method: 'POST', params: {}, isArray: false }
            });
            t.postM(savedata).$promise.then(
                function (value) { $location.path('/'); },
                function (error) { /*Do something with error*/ }
            );
            
        } else {
            console.log("GOING TO UPDATE");
            t = $resource(MainUrlWithId, {
                id: savedata.TodoId
            }, { update: { method: 'PUT', data: {}, isArray: false } });


            var updateData = {
                TodoId: savedata.TodoId,
                Name: savedata.Name,
                Content: savedata.Content,
                DueDateTime: savedata.DueDateTime,
                IsCompleted: savedata.IsCompleted
            };

            t.update(updateData).$promise.then(
                function(value) {  $location.path('/'); },
                function(error) { /*Do something with error*/ }
            );
        }
    }

    $scope.message = 'Todo List Edit Page';
}]);

todoApp.controller('aboutController', function ($scope) {
    $scope.message = 'This is a Todo List Application';
});

