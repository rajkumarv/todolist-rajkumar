﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

INSERT INTO [dbo].[Users]
           ([UserId]
           ,[Username]
           ,[Password]
           ,[Email])
     VALUES
           ('D1BAEA25-DA56-43C5-AE2E-DC1C2FE0B727'
		   ,'rajv'
           ,'Password@1'
           ,'RajkumarV.us@gmail.com'
           )
GO