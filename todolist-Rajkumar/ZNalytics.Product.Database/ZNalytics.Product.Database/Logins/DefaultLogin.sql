﻿-- Creates the login AbolrousHazem with password '340$Uuxwp7Mcxo7Khy'.
CREATE LOGIN sa 
    WITH PASSWORD = 'sa';
GO

-- Creates a database user for the login created above.
CREATE USER sa FOR LOGIN sa;
GO