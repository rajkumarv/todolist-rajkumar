﻿using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using ZNalytics.Product.Common.BusinessLogic.ManagerProxies;
using ZNalytics.Product.Common.BusinessLogic.Models;
using ZNalytics.Product.Common.Controllers;
using ZNalytics.Product.Common.Factories;
using ZNalytics.Product.Common.Service;

namespace ZNalytics.Product.Common.API.Controllers
{
    public class AuthenticateController : WebApiController
    {
        // POST api/Authenticate
        public HttpResponseMessage Post([FromBody]LoginParams loginParams)
        {
            var authenticateManagerProxy = BusinessLogicManagerProxyFactory<AuthenticateManagerProxy>.Create(FirstServiceContext);
            return authenticateManagerProxy.GetAuthenticationToken(loginParams);
        }
    }
}
