﻿using System.Web;
using System.Web.Http;

namespace ZNalytics.Product.Common.API
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
