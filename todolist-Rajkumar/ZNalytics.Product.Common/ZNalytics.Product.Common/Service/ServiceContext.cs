﻿using System;
using System.Collections.Generic;
using System.Configuration;
using PetaPoco;
using ZNalytics.Product.Common.DataAccess;
using ZNalytics.Product.Common.Security;

namespace ZNalytics.Product.Common.Service
{
    public class ServiceContext
    {
        public ProductDatabase ProductDatabase { get; set; }
        public SecurityContext SecurityContext { get; set; }

        public string WebHostName { get; set; }

        public int WebHostPort { get; set; }

        public ConnectionStringSettingsCollection DatabaseConnectionSettings { get; set; }

        public void Load()
        {
            WebHostName = ConfigurationManager.AppSettings["WebHostName"];
            var port = 2020;
            if (!Int32.TryParse(ConfigurationManager.AppSettings["WebHostPort"], out port))
            {
                port = 2020;
            }
            WebHostPort = port;
            DatabaseConnectionSettings = ConfigurationManager.ConnectionStrings;
            ProductDatabase = new ProductDatabase(new Database("SQLServer"));
        }
    }
}
