﻿namespace ZNalytics.Product.Common.Interfaces
{
    public interface IManagerTestBase
    {
        void OnSetup();
        void SetupMocks();
        void OnCleanup();
    }
}
