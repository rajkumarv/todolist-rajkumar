﻿using System;
using ZNalytics.Product.Common.Interfaces;
using ZNalytics.Product.Common.Service;

namespace ZNalytics.Product.Common.BusinessLogic
{
    public abstract class BuninessLogicManagerBase : IBusinessLogicManager, IDisposable
    {
        private bool _disposed;

        public ServiceContext CurrentServiceContext { get; set; }

        protected BuninessLogicManagerBase(ServiceContext currentServiceContext)
        {
            if (currentServiceContext == null)
            {
                throw new ArgumentNullException("currentServiceContext");
            }

            CurrentServiceContext = currentServiceContext;
        }
        ~BuninessLogicManagerBase()
        {
            Dispose(false);
        }

        public virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

}
