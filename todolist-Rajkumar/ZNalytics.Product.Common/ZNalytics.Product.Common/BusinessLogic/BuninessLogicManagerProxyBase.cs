﻿using System;
using System.CodeDom;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using Newtonsoft.Json;
using ZNalytics.Product.Common.Interfaces;
using ZNalytics.Product.Common.Responses;
using ZNalytics.Product.Common.Service;

namespace ZNalytics.Product.Common.BusinessLogic
{
    public abstract class BuninessLogicManagerProxyBase : IBusinessLogicManagerProxy, IDisposable
    {
        private bool _disposed;

        public ServiceContext CurrentServiceContext { get; set; }

        public HttpResponseMessage CreateWebApiResponse(object data, object errors)
        {
            var response = new HttpResponseMessage
            {
                Content = new ObjectContent<WebApiResponse>(
                    data == null
                        ? new WebApiResponse
                        {
                            Data = null,
                            Errors = errors,
                            StatusCode = HttpStatusCode.InternalServerError
                        }
                        : new WebApiResponse()
                        {
                            Data = data,
                            Errors = null,
                            StatusCode = HttpStatusCode.OK
                        },
                    new JsonMediaTypeFormatter(), "application/json")
            };
            return response;
        }

        protected BuninessLogicManagerProxyBase(ServiceContext currentServiceContext)
        {
            if (currentServiceContext == null)
            {
                throw new ArgumentNullException("currentServiceContext");
            }

            CurrentServiceContext = currentServiceContext;
        }
        ~BuninessLogicManagerProxyBase()
        {
            Dispose(false);
        }

        public virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

}
