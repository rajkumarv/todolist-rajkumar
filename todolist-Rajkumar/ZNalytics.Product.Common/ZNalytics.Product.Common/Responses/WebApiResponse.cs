﻿using System.Net.Http;

namespace ZNalytics.Product.Common.Responses
{
    public class WebApiResponse : HttpResponseMessage
    {
        
        public object Data { set; get; }
        public object Errors { set; get; }   
     
    }
}
