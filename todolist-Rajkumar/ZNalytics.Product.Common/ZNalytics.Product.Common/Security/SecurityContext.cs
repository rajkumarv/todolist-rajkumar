﻿using System;

namespace ZNalytics.Product.Common.Security
{
    public class SecurityContext
    {
        public Guid UserId { get; set; }

        public string SessionKey { get; set; }
    }
}
