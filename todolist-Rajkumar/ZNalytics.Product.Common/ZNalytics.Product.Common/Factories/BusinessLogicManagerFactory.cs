﻿using System;
using System.Reflection;
using ZNalytics.Product.Common.BusinessLogic;
using ZNalytics.Product.Common.Service;

namespace ZNalytics.Product.Common.Factories
{
    public static class BusinessLogicManagerFactory<TBusinessLogicManager> where TBusinessLogicManager : BuninessLogicManagerBase
    {
        public static TBusinessLogicManager Create(ServiceContext serviceContext)
        {
            if (serviceContext == null) throw new ArgumentNullException("serviceContext");

            const BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Instance;
            var ctor = typeof(TBusinessLogicManager).GetConstructor(flags, null, new Type[] { typeof(ServiceContext) }, null);

            return (TBusinessLogicManager)ctor.Invoke(new object[] { serviceContext });
        }
    }
}
