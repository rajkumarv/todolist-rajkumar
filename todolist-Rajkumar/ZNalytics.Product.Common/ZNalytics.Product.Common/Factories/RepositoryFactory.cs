﻿using System;
using System.Reflection;
using ZNalytics.Product.Common.DataAccess;
using ZNalytics.Product.Common.Interfaces;
using ZNalytics.Product.Common.Service;

namespace ZNalytics.Product.Common.Factories
{
    public static class RepositoryFactory<TRepositoryBase> where TRepositoryBase : class, IRepository
    {
        public static TRepositoryBase Create(ServiceContext serviceContext)
        {
            if (serviceContext == null) throw new ArgumentNullException("serviceContext");

            const BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Instance;
            var ctor = typeof(TRepositoryBase).GetConstructor(flags, null, new Type[] { typeof(ServiceContext) }, null);

            return (TRepositoryBase)ctor.Invoke(new object[] { serviceContext });
        }
    }
}
