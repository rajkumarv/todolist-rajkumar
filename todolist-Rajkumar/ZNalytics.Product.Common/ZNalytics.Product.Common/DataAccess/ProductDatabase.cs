﻿using System;
using System.Collections.Generic;
using PetaPoco;

namespace ZNalytics.Product.Common.DataAccess
{
    public class ProductDatabase
    {
        private readonly Database _database;

        public ProductDatabase(Database database)
        {
            _database = database;
        }

        #region Execute

        public int Execute(string sql, params object[] args)
        {
            return _database.Execute(sql, args);
        }

        public int Execute(Sql sql)
        {
            return _database.Execute(sql);
        }

        public T ExecuteScalar<T>(string sql, params object[] args)
        {
            return _database.ExecuteScalar<T>(sql, args);
        }

        public T ExecuteScalar<T>(Sql sql)
        {
            return _database.ExecuteScalar<T>(sql);
        }

        #endregion

        #region Insert

        public object Insert(object poco)
        {
            return _database.Insert(poco);
        }

        #endregion

        #region Update

        public int Update(object poco)
        {
            return _database.Update(poco);
        }

        public int Update(object poco, object primaryKey)
        {
            return _database.Update(poco);
        }

        #endregion

        #region Fetch

        public List<T> Fetch<T>(Sql sql)
        {
            return _database.Fetch<T>(sql);
        }

        public List<T> Fetch<T>(long page, long itemsPerPage, string sql, params object[] args)
        {
            return _database.Fetch<T>(page, itemsPerPage, sql, args);
        }

        public List<T> Fetch<T>(long page, long itemsPerPage, Sql sql)
        {
            return _database.Fetch<T>(page, itemsPerPage, sql);
        }

        // Multi Fetch
        public List<TRet> Fetch<T1, T2, TRet>(Func<T1, T2, TRet> cb, string sql, params object[] args)
        {
            return _database.Fetch<T1, T2, TRet>(cb, sql, args);
        }

        public List<TRet> Fetch<T1, T2, T3, TRet>(Func<T1, T2, T3, TRet> cb, string sql, params object[] args)
        {
            return _database.Fetch<T1, T2, T3, TRet>(cb, sql, args);
        }

        public List<TRet> Fetch<T1, T2, T3, T4, TRet>(Func<T1, T2, T3, T4, TRet> cb, string sql, params object[] args)
        {
            return _database.Fetch<T1, T2, T3, T4, TRet>(cb, sql, args);
        }

        // Multi Fetch (SQL builder)
        public List<TRet> Fetch<T1, T2, TRet>(Func<T1, T2, TRet> cb, Sql sql)
        {
            return _database.Fetch<T1, T2, TRet>(cb, sql.SQL, sql.Arguments);
        }

        public List<TRet> Fetch<T1, T2, T3, TRet>(Func<T1, T2, T3, TRet> cb, Sql sql)
        {
            return _database.Fetch<T1, T2, T3, TRet>(cb, sql.SQL, sql.Arguments);
        }

        public List<TRet> Fetch<T1, T2, T3, T4, TRet>(Func<T1, T2, T3, T4, TRet> cb, Sql sql)
        {
            return _database.Fetch<T1, T2, T3, T4, TRet>(cb, sql.SQL, sql.Arguments);
        }

        // Multi Fetch (Simple)
        public List<T1> Fetch<T1, T2>(string sql, params object[] args)
        {
            return _database.Fetch<T1, T2>(sql, args);
        }

        public List<T1> Fetch<T1, T2, T3>(string sql, params object[] args)
        {
            return _database.Fetch<T1, T2, T3>(sql, args);
        }

        public List<T1> Fetch<T1, T2, T3, T4>(string sql, params object[] args)
        {
            return _database.Fetch<T1, T2, T3, T4>(sql, args);
        }

        // Multi Fetch (Simple) (SQL builder)
        public List<T1> Fetch<T1, T2>(Sql sql)
        {
            return _database.Fetch<T1, T2>(sql.SQL, sql.Arguments);
        }

        public List<T1> Fetch<T1, T2, T3>(Sql sql)
        {
            return _database.Fetch<T1, T2, T3>(sql.SQL, sql.Arguments);
        }

        public List<T1> Fetch<T1, T2, T3, T4>(Sql sql)
        {
            return _database.Fetch<T1, T2, T3, T4>(sql.SQL, sql.Arguments);
        }

        #endregion

        #region Single

        public T Single<T>(object primaryKey)
        {
            return _database.Single<T>(primaryKey);
        }

        public T SingleOrDefault<T>(object primaryKey)
        {
            return _database.SingleOrDefault<T>(primaryKey);
        }

        public T Single<T>(string sql, params object[] args)
        {
            return _database.Single<T>(sql, args);
        }

        public T Single<T>(Sql sql)
        {
            return _database.Single<T>(sql);
        }

        #endregion

        #region SingleOrDefault

        public T SingleOrDefaultByPrimaryKey<T>(object primaryKey)
        {
            return
                _database.SingleOrDefault<T>(
                    string.Format("WHERE {0}=@0",
                        _database.EscapeSqlIdentifier(Database.PocoData.ForType(typeof (T)).TableInfo.PrimaryKey)),
                    primaryKey);
        }

        public T SingleOrDefault<T>(string sql, params object[] args)
        {
            return _database.SingleOrDefault<T>(sql, args);
        }

        public T SingleOrDefault<T>(Sql sql)
        {
            return _database.SingleOrDefault<T>(sql);
        }

        #endregion

        #region Exists

        public bool Exists<T>(object primaryKey)
        {
            return _database.Exists<T>(primaryKey);
        }

        #endregion

        #region First

        public T First<T>(string sql, params object[] args)
        {
            return _database.First<T>(sql, args);
        }

        public T First<T>(Sql sql)
        {
            return _database.First<T>(sql);
        }

        #endregion

        #region FirstOfDefault

        public T FirstOrDefault<T>(string sql, params object[] args)
        {
            return _database.FirstOrDefault<T>(sql, args);
        }

        public T FirstOrDefault<T>(Sql sql)
        {
            return _database.FirstOrDefault<T>(sql);
        }

        #endregion

        #region Page

        public void BuildPageQueries<T>(long skip, long take, string sql, ref object[] args, out string sqlCount,
            out string sqlPage)
        {
            _database.BuildPageQueries<T>(skip, take, sql, ref args, out sqlCount, out sqlPage);
        }

        public Page<T> Page<T>(long page, long itemsPerPage, string sql, params object[] args)
        {
            return _database.Page<T>(page, itemsPerPage, sql, args);
        }

        public Page<T> Page<T>(long page, long itemsPerPage, Sql sql)
        {
            return _database.Page<T>(page, itemsPerPage, sql);
        }

        #endregion

        #region Transaction

        public Transaction GetTransaction()
        {
            return _database.GetTransaction();
        }

        public void BeginTransaction()
        {
            _database.BeginTransaction();
        }

        public void AbortTransaction()
        {
            _database.AbortTransaction();
        }

        public void CompleteTransaction()
        {
            _database.CompleteTransaction();
        }

        #endregion
    }
}