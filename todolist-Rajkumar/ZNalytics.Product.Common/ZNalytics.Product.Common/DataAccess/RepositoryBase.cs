﻿    using System;
    using ZNalytics.Product.Common.Interfaces;
    using ZNalytics.Product.Common.Service;

namespace ZNalytics.Product.Common.DataAccess
{
    public abstract class RepositoryBase : IRepository, IDisposable
    {
        private bool _disposed;

        public ServiceContext CurrentServiceContext { get; protected set; }

        protected RepositoryBase(ServiceContext currentServiceContext)
        {
            if (currentServiceContext == null)
            {
                throw new ArgumentNullException("currentServiceContext");
            }

            CurrentServiceContext = currentServiceContext;
        }
        ~RepositoryBase()
        {
            Dispose(false);
        }

        public virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
