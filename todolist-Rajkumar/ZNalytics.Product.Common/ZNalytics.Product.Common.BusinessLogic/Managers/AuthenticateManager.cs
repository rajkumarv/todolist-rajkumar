﻿using System.Configuration;
using System.Linq;
using ZNalytics.Product.Common.BusinessLogic.Models;
using ZNalytics.Product.Common.DataAccess.Repositories;
using ZNalytics.Product.Common.Factories;
using ZNalytics.Product.Common.Service;

namespace ZNalytics.Product.Common.BusinessLogic.Managers
{
    public class AuthenticateManager : BuninessLogicManagerBase
    {
        private AuthenticateManager(ServiceContext currentServiceContext): base(currentServiceContext)
        {
        }
        public LoginParams GetAuthenticationToken(LoginParams loginParams)
        {
            loginParams.SessionToken = "Authenticated";
            var authenticateRpositoryProxy = RepositoryFactory<UserRepository>.Create(CurrentServiceContext);
            var users = authenticateRpositoryProxy.GetByUserNameAndPassword(loginParams.UserName,loginParams.Password);
            loginParams.SessionToken = users.Any() ? "Authenticated" : "Unauthorized";
            return loginParams;
        }
    }
}
