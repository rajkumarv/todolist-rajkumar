﻿using System;
using System.Net.Http;
using ZNalytics.Product.Common.BusinessLogic.Managers;
using ZNalytics.Product.Common.BusinessLogic.Models;
using ZNalytics.Product.Common.Factories;
using ZNalytics.Product.Common.Service;

namespace ZNalytics.Product.Common.BusinessLogic.ManagerProxies
{
    public class AuthenticateManagerProxy : BuninessLogicManagerProxyBase
    {
        private readonly AuthenticateManager _authenticateManager;
        private AuthenticateManagerProxy(ServiceContext currentServiceContext): base(currentServiceContext)
        {
            _authenticateManager = BusinessLogicManagerFactory<AuthenticateManager>.Create(currentServiceContext);
        }

        public HttpResponseMessage GetAuthenticationToken(LoginParams loginParams)
        {
            try
            {
                return CreateWebApiResponse(_authenticateManager.GetAuthenticationToken(loginParams), null);
            }
            catch (Exception ex)
            {
                return CreateWebApiResponse(null,ex);
            }
        }
    }
}