﻿namespace ZNalytics.Product.Common.BusinessLogic.Models
{
    public class LoginParams
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string SessionToken { get; set; }
    }
}