﻿using System;
using PetaPoco;

namespace ZNalytics.Product.Common.DataAccess.RepositoryObjects
{
    [TableName("Users")]
    [PrimaryKey("UserId", autoIncrement = false)]
    public class User
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Passsword { get; set; }
        public string Email { get; set; }
        public Boolean IsDeleted { get; set; }
    }
}
