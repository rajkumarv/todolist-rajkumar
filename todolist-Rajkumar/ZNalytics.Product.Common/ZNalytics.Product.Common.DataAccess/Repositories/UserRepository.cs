﻿using System.Collections.Generic;
using System.Data.SqlClient;
using PetaPoco;
using ZNalytics.Product.Common.DataAccess.RepositoryObjects;
using ZNalytics.Product.Common.Service;

namespace ZNalytics.Product.Common.DataAccess.Repositories
{
    public class UserRepository : RepositoryBase
    {
        private UserRepository(ServiceContext currentServiceContext) : base(currentServiceContext)
        {
        }
        public List<User> Get()
        {
            return CurrentServiceContext.ProductDatabase.Fetch<User>(Sql.Builder
                .Select("*")
                .From("Users"));
        }
        public List<User> GetByUserNameAndPassword(string username, string password)
        {
            return CurrentServiceContext.ProductDatabase.Fetch<User>(Sql.Builder
                .Select("*")
                .From("Users")
                .Where("UserName = @0", username)
                .Where("Password = @0", password)
                );
        }
    }
}
