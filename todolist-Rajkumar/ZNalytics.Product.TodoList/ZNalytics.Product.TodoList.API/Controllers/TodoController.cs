﻿using System;
using System.Net.Http;
using System.Web.Http;
using ZNalytics.Product.Common.Controllers;
using ZNalytics.Product.Common.Factories;
using ZNalytics.Product.TodoList.BusinessLogic.ManagerProxies;
using ZNalytics.Product.TodoList.BusinessLogic.Models;

namespace ZNalytics.Product.TodoList.API.Controllers
{
    public class TodoController : WebApiController
    {
        // GET api/Todo
        [HttpGet]
        public HttpResponseMessage Get()
        {
            var todoManagerProxy = BusinessLogicManagerProxyFactory<TodoManagerProxy>.Create(FirstServiceContext);
            return todoManagerProxy.GetAllTodos();
        }

        // GET api/Todo/(Guid)
        [HttpGet]
        public HttpResponseMessage Get(Guid id)
        {
            var todoManagerProxy = BusinessLogicManagerProxyFactory<TodoManagerProxy>.Create(FirstServiceContext);
            return todoManagerProxy.GetTodoById(id);
        }

        // POST api/Todo
        [HttpPost]
        public HttpResponseMessage Post([FromBody]Todo todo)
        {
            var todoManagerProxy = BusinessLogicManagerProxyFactory<TodoManagerProxy>.Create(FirstServiceContext);
            return todoManagerProxy.InsertTodo(todo);
        }

        // PUT api/Todo
        [HttpPut]
        public HttpResponseMessage Put(Guid id, [FromBody]Todo todo)
        {
            var todoManagerProxy = BusinessLogicManagerProxyFactory<TodoManagerProxy>.Create(FirstServiceContext);
            return todoManagerProxy.UpdateTodoById(id, todo);
        }

        // DELETE api/Todo/(Guid)
        [HttpDelete]
        public HttpResponseMessage Delete(Guid id)
        {
            var todoManagerProxy = BusinessLogicManagerProxyFactory<TodoManagerProxy>.Create(FirstServiceContext);
            return todoManagerProxy.DeleteTodo(id);
        }
    }
}
