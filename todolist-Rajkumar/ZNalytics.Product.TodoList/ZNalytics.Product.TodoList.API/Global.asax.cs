﻿using System.Web;
using System.Web.Http;
using ZNalytics.Product.TodoList.API.Handlers;

namespace ZNalytics.Product.TodoList.API
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configuration.MessageHandlers.Add(new CorsHandler());
        }
    }
}
