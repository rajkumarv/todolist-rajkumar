﻿using System;
using System.Net.Http;
using ZNalytics.Product.Common.BusinessLogic;
using ZNalytics.Product.Common.Factories;
using ZNalytics.Product.Common.Service;
using ZNalytics.Product.TodoList.BusinessLogic.Managers;
using ZNalytics.Product.TodoList.BusinessLogic.Models;

namespace ZNalytics.Product.TodoList.BusinessLogic.ManagerProxies
{
    public class TodoManagerProxy : BuninessLogicManagerProxyBase
    {
        private readonly TodoManager _todoManager;

        private TodoManagerProxy(ServiceContext currentServiceContext) : base(currentServiceContext)
        {
            _todoManager = BusinessLogicManagerFactory<TodoManager>.Create(currentServiceContext);
        }

        public HttpResponseMessage GetAllTodos()
        {
            try
            {
                return CreateWebApiResponse(_todoManager.GetAllTodos(), null);
            }
            catch (Exception ex)
            {
                return CreateWebApiResponse(null, ex);
            }
        }

        public HttpResponseMessage GetTodoById(Guid todoId)
        {
            try
            {
                return CreateWebApiResponse(_todoManager.GetTodoById(todoId), null);
            }
            catch (Exception ex)
            {
                return CreateWebApiResponse(null, ex);
            }
        }

        public HttpResponseMessage InsertTodo(Todo todo)
        {
            try
            {
                return CreateWebApiResponse(_todoManager.InsertTodo(todo), null);
            }
            catch (Exception ex)
            {
                return CreateWebApiResponse(null, ex);
            }
        }

        public HttpResponseMessage UpdateTodoById(Guid id, Todo todo)
        {
            try
            {
                return CreateWebApiResponse(_todoManager.UpdateTodoById(id, todo), null);
            }
            catch (Exception ex)
            {
                return CreateWebApiResponse(null, ex);
            }
        }

        public HttpResponseMessage DeleteTodo(Guid todoId)
        {
            try
            {
                return CreateWebApiResponse(_todoManager.DeleteTodoById(todoId), null);
            }
            catch (Exception ex)
            {
                return CreateWebApiResponse(null, ex);
            }
        }
    }
}
