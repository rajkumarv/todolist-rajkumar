﻿using System;

namespace ZNalytics.Product.TodoList.BusinessLogic.Models
{
    public class Todo
    {
        public Guid TodoId { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public DateTimeOffset DueDateTime { get; set; }
        public Boolean IsCompleted { get; set; }
    }
}
