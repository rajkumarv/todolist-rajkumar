﻿using System;
using System.Collections.Generic;
using System.Linq;
using PetaPoco;
using ZNalytics.Product.Common.DataAccess;
using ZNalytics.Product.Common.DataAccess.Repositories;
using ZNalytics.Product.Common.Factories;
using ZNalytics.Product.Common.Service;
using ZNalytics.Product.TodoList.DataAccess.Interfaces;
using ZNalytics.Product.TodoList.DataAccess.RepositoryObjects;

namespace ZNalytics.Product.TodoList.DataAccess.Repositories
{
    public class TodoRepository : RepositoryBase, ITodoRepository
    {
        private TodoRepository(ServiceContext serviceContext) : base(serviceContext)
        {
        }

        public Todo InsertTodo(Todo todo)
        {
            var userRpositoryProxy = RepositoryFactory<UserRepository>.Create(CurrentServiceContext);
            var users = userRpositoryProxy.Get();

            if (users.Any())
            {
                todo.CreatedUserId = users.First().UserId;
                todo.ModifiedUserId = users.First().UserId;

            }
            else throw new NotImplementedException("No User found. Add a user.");
            var id = CurrentServiceContext.ProductDatabase.Insert(todo);
            return todo;
        }

        public List<Todo> GetAllTodos()
        {
            return CurrentServiceContext.ProductDatabase.Fetch<Todo>(
                Sql.Builder
                    .Select("*")
                    .From("Todos")
                    .Where("IsDeleted = 0"));
        }

        public Todo GetByTodoId(Guid todoId)
        {
            return CurrentServiceContext.ProductDatabase.SingleOrDefault<Todo>(
                Sql.Builder
                    .Select("*")
                    .From("Todos")
                    .Where("TodoId = @0", todoId)
                    .Where("IsDeleted = 0"));
        }

        public Todo UpdateByTodoId(Guid todoId, Todo todo)
        {
            if (todoId == Guid.Empty) throw new ArgumentOutOfRangeException("todoId cannot be Guid.Empty.");
            todo.TodoId = todoId;

            CurrentServiceContext.ProductDatabase.Execute(
                Sql.Builder
                    .Append("UPDATE Todos SET Name = @0", todo.Name)
                    .Append(" , Content = @0", todo.Content)
                    .Append(" , DueDateTime = @0", todo.DueDateTime)
                    .Append(" , IsCompleted = @0", todo.IsCompleted)
                    .Append(" WHERE TodoId = @0", todoId));
            return GetByTodoId(todoId);
        }

        public int DeleteByTodoId(Guid todoId)
        {
            if (todoId == Guid.Empty) throw new ArgumentOutOfRangeException("todoId cannot be Guid.Empty.");
            return CurrentServiceContext.ProductDatabase.Execute(
                Sql.Builder
                    .Append("UPDATE Todos SET IsDeleted = 1 WHERE TodoId = @0", todoId));
        }
    }
}