﻿using System;
using PetaPoco;

namespace ZNalytics.Product.TodoList.DataAccess.RepositoryObjects
{
    [TableName("Todos")]
    [PrimaryKey("TodoId", autoIncrement = false)]
    public class Todo
    {
        public Guid TodoId { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public DateTimeOffset DueDateTime { get; set; }
        [Ignore]
        public DateTimeOffset CreatedDateTime { get; set; }
        public DateTimeOffset ModifiedDateTime { get; set; }
        public Guid CreatedUserId { get; set; }
        public Guid ModifiedUserId { get; set; }
        public Boolean IsDeleted { get; set; }
        public Boolean IsCompleted { get; set; }
    }
}
